﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Audit.aspx.cs" Inherits="MultiLoans.Audit" %>

<%@ Register Src="~/Controls/AuditSheetUC.ascx" TagName="AuditControl" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" style="margin-top: 100px;">
        <h3>Audyt kredytowy jako bezpłatne narzędzie zawiera:</h3>

        <ul>
            <li>informacje na temat obecnej realizacji zobowiązań,</li>
            <li>przedstawienie krótkiej analizy oraz rekomendacji do zastosowania w celu</li>
            <li>poprawy sytuacji ekonomicznej biznesu.</li>
        </ul>
        <br />

        <p>
            Audyt wymaga wypełnienia podstawowych danych w formie formularza. Nasz analityk na
podstawie otrzymanych informacji przygotowuje mini raport, który otrzymają Państwo mailowo
lub telefonicznie.
        </p>
        <div class="container" style="text-align: center">
            <asp:Button ID="btnOpenAudit" runat="server" CssClass="btn btn-primary" Width="150px" Text="Zgłoś audyt" OnClick="btnOpenAudit_Click" />
        </div>
        <br />
            <h3>Istnieje również możliwość szczegółowiej analizy zobowiązań, wówczas audyt jest odpłatny 
(koszt 59,90 PLN netto + VAT), który obejmuje następujące etapy:</h3>
            
        <ul>
            <li>analiza zapisów umów kredytowych / leasingowych etc. pod kątem oceny przyjętych
                zabezpieczeń oraz warunków cenowych,</li>
            <li>ocena warunków umów o prowadzenie rachunków bankowych,</li>
            <li>przygotowanie raportu z aktualnych warunków umów zawartych z instytucjami
                finansowymi,
            </li>
            <li>określenie produktów / usług finansowych potrzebnych Klientowi,</li>
            <li>przedstawienie możliwości optymalizacyjnych czynnych transakcji finansowych,</li>
            <li>dopasowanie produktów finansowych oraz źródeł finansowania do potrzeb i możliwości  
                Klienta,
            </li>
            <li>przeprowadzenie renegocjacji z instytucjami finansowymi w zakresie poprawy warunków
                cenowych zawartych umów,
            </li>
            <li>pozyskanie finansowania bankowego lub hybrydowego na korzystniejszych warunkach
                cenowych lub lepiej dopasowanych do Twoich potrzeb
            </li>
        </ul>
        <br />
        <h3>Chcesz rozwinąć swój biznes? Możemy zaoferować Ci instrumenty wsparcia biznesu,
        których koszt jest ustalany indywidualnie w zależności od produktu:
        </h3>
        <p>
            <b>Executive coaching</b> – forma współpracy polegająca na kilkugodzinnym spotkaniu
indywidualnym lub cyklu spotkań z właścicielami lub przedstawicielami kadry zarządzającej
        <br />

            <b>Szkolenia</b> – przekazanie wiedzy i narzędzi w wybranym zakresie w trakcie 1-3 dniowego
szkolenia; przekazana wiedza i narzędzia są wykorzystywane we własnym zakresie przez Klienta
        <br />
            <b>Warsztaty</b> – przekazanie wiedzy i narzędzi w wybranym zakresie połączone z pracą nad
konkretnym studium przypadku Klienta podczas zaplanowanych sesji warsztatowych, przy
zapewnieniu koordynacji pracy zespołu pomiędzy sesjami warsztatowymi – czas trwania w
zależności od wybranej tematyki<br />

            <b>Projekt doradczy</b> – przeprowadzenie niezbędnych analiz, przy czym prace merytoryczne nad
projektem odbywają się przy ograniczonym zaangażowaniu czasowym po stronie Klienta – udział
Klienta wymagany jest na etapie podejmowania decyzji
        <br />
            <b>Interim management</b> – najbardziej zaawansowana forma współpracy związana z
merytorycznym przygotowaniem projektu oraz uczestniczeniem we wdrażaniu zaplanowanych
zmian
        </p>
        <br />


    </div>
</asp:Content>
