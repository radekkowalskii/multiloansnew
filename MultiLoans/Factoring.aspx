﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Factoring.aspx.cs" Inherits="MultiLoans.Factoring" %>

<%@ Register Src="~/Controls/ContactForm.ascx" TagName="ContactForm" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--    <style>
        .bottom-left {
    position: absolute;
    bottom: 8px;
    left: 16px;
}
.container {
    position: relative;
    text-align: center;
    color: white;
}
/* Top left text */
.top {
    position: absolute;
    top: 8px;
    text-align:center;
    margin:0 auto;
    padding: 0px 10px 0px 10px;
    width: 80%;
    left:20%;
    right:20%;
}

/* Top right text */
        .top-right {
            position: absolute;
            top: 8px;
            right: 16px;
        }
/* Bottom right text */
        .bottom-right {
            position: absolute;
            bottom: 8px;
            right: 16px;
            color:
        }
/* Centered text */
.centered {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    
}


/*ssssss*/
.h1 {
  font-size: 33px;
    font-weight: bold;
  text-align: center;
  line-height:1.5;
  color:#FDBA03;
  margin-top:40px;
}
.h2 {
  font-size: 25px;
  font-weight: 400;
  color:#d4d4c3;
  letter-spacing: -0.03em;
  margin-bottom: 0.571em;
  margin-top: 1em;
  text-align: center;
  line-height:1.5;
}
.jumbotron{
    background-image: url('img/factoring1.jpg');
   
    background-size: cover;
    min-height:750px;
    color:#FDBA03;
}

.my-sub-title {
     margin-top: 2.0em;
  font-weight: 400;
  text-align: center;
  color:#d4d4c3;
}

h2 span { 
}

h3 span { 
   
}
    </style>--%>

    <%--<div class="container" style="margin-top:150px; margin-bottom:60px;">
        <img src="img/factoring1.jpg" style="width:100%">
            <div class="top">Przedsiębiorco pamiętaj faktoring to elastyczne rozwiązanie finansowe dzięki któremu otrzymasz pieniądze natychmiast po dokonaniu sprzedaży bez zbędnego zaciągania długoterminowych zobowiązań.
Z Multi Loans uzyskasz limit faktoringowy, jak również sprzedaż jednostkową fakturę , dzięki naszej platformie otrzymasz nawet  do 96% wartości faktury.</div>
        </img>
         <%--<div class="bottom-left">Bottom Left</div>
  <div class="top">Przedsiębiorco pamiętaj faktoring to elastyczne rozwiązanie finansowe dzięki któremu otrzymasz pieniądze natychmiast po dokonaniu sprzedaży bez zbędnego zaciągania długoterminowych zobowiązań.
Z Multi Loans uzyskasz limit faktoringowy, jak również sprzedaż jednostkową fakturę , dzięki naszej platformie otrzymasz nawet  do 96% wartości faktury.


</div>
  <div class="bottom-right">Bottom Right</div>
  <div class="centered">Nie zwlekaj wypełnij formularz i ciesz się gotówką bez terminów płatności.</div>--%>
    <%--</div>--%>

    <%-- <div id="front-landing">
   <div class="container jumbotron" style="margin-top:150px; margin-bottom:60px;">
       <h2 class="h1"><span>Przedsiębiorco! </span></h2>
       <h2 class="h2"><span>Faktoring to elastyczne rozwiązanie finansowe dzięki któremu otrzymasz pieniądze natychmiast po dokonaniu sprzedaży bez zbędnego zaciągania długoterminowych zobowiązań.</span></h2>
       <h3 class="my-sub-title">
          <span>Z MultiLoans uzyskasz limit faktoringowy, jak również sprzedaż jednostkową fakturę , dzięki naszej platformie otrzymasz nawet  do 96% wartości faktury.</span>
       </h3>
       <div style="margin-top:50px;">
           <h2 class="h1"><span>Wypełnij formularz </span></h2>
           <uc:FactoringSheet runat="server"/>
       </div>
    </div>
</div>--%>
    <!-- Alt Services-->
    <section class="module module-divider-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center">
                        <h2>Faktoring</h2>
                        <p class="font-serif">
                            Faktoring to elastyczne rozwiązanie finansowe dzięki któremu otrzymasz pieniądze natychmiast po dokonaniu sprzedaży bez zbędnego zaciągania długoterminowych zobowiązań.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="m-t-30"></div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-upload"></span></div>
                        <div class="icon-box-title">
                            <h6>Faktoring z regresem</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-target"></span></div>
                        <div class="icon-box-title">
                            <h6>Faktoring bez regresu</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-signs"></span></div>
                        <div class="icon-box-title">
                            <h6>Faktoring eksportowy</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 hidden-sm-down">
                    <div class="text-center">
                        <img src="img/factoring_image1.jpg" alt="">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="m-t-30"></div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-life-buoy"></span></div>
                        <div class="icon-box-title">
                            <h6>Faktoring dla transportu</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-calculator"></span></div>
                        <div class="icon-box-title">
                            <h6>Faktoring dla małych firm</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-link"></span></div>
                        <div class="icon-box-title">
                            <h6>Faktoring odwrócony</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Alt Services end-->

    <div style="margin-top:50px"">
        <h3 style=" text-align: center">Wypełnij formularz </h3>
        <uc:ContactForm runat="server" />
    </div>
</asp:Content>
