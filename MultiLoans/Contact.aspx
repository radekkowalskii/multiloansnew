﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Contact.aspx.cs" Inherits="MultiLoans.Contact" %>
<%@ Register Src="~/Controls/ContactForm.ascx" TagName="ContactForm" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   <%--<div class="jumbotron" style="margin-top:100px">
    <h3>Zapraszamy do kontaktu!</h3>
    
    <address>
        ul. Ulica 22<br />
        00-000 Warszawa<br />
       <span class="glyphicon glyphicon-earphone"></span><a href="tel:+000000000">000000000</a>    

    </address>

       <h3>Wyślij wiadomość e-mail</h3>
    <address>
        <strong>Helpdesk:</strong>   <a href="mailto:Support@example.com">Helpdesk@example.com</a><br />
        <strong>Marketing:</strong> <a href="mailto:Marketing@example.com">Marketing@example.com</a>
    </address>
       <h3>Możesz też wysłać prośbę o pilny kontakt:</h3>
       <uc:ContactForm ID="ContactForm" runat="server"></uc:ContactForm>
       </div>--%>

    <!-- Wrapper-->
			<div class="wrapper">

				<!-- Map-->
				<section class="maps-container">
					<div class="map" data-addresses="[52.229676, 21.012229]" data-info="[Pałac kultury i nauki]" data-icon="assets/images/map-icon.png" data-zoom="15"></div>
				</section>
				<!-- Map end-->

				<!-- Contact-->
				<section class="module">
					<div class="container">
						<div class="row">
							<div class="col-md-8">
								<form class="m-b-60" id="contact-form" method="post" novalidate>
									<div class="row">
										<div class="col-md-4">
											<div class="form-group">
												<input class="form-control" type="text" name="name" placeholder="Name" required="">
												<p class="help-block text-danger"></p>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<input class="form-control" type="email" name="email" placeholder="E-mail" required="">
												<p class="help-block text-danger"></p>
											</div>
										</div>
										<div class="col-md-4">
											<div class="form-group">
												<input class="form-control" type="text" name="subject" placeholder="Subject" required="">
												<p class="help-block text-danger"></p>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<textarea class="form-control" name="message" placeholder="Message" rows="12" required=""></textarea>
											</div>
										</div>
										<div class="col-md-12">
											<input class="btn btn-round btn-brand" type="submit" value="Send Request">
										</div>
									</div>
								</form>
								<!-- Ajax response-->
								<div class="ajax-response text-center" id="contact-response"></div>
							</div>
							<div class="col-md-4">
								<div class="icon-box icon-box-left p-t-0">
									<div class="icon-box-icon"><span class="icon icon-basic-paperplane"></span></div>
									<div class="icon-box-title">
										<h6>Say Hello</h6>
									</div>
									<div class="icon-box-content">
										<p>Email: multiloans@contact.com <br/> Phone: +1 234 567 89 10</p>
									</div>
								</div>
								<div class="icon-box icon-box-left">
									<div class="icon-box-icon"><span class="icon icon-basic-map"></span></div>
									<div class="icon-box-title">
										<h6>Meeting place</h6>
									</div>
									<div class="icon-box-content">
										<p>Multiloans Centre, <br/> Plac Defilad, Warsaw</p>
									</div>
									<div class="icon-box-link"><a href="#">Find us on map &rarr;</a></div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!-- Contact end-->
                </div>
</asp:Content>
