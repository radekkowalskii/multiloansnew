﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SecuredLoan.aspx.cs" Inherits="MultiLoans.SecuredLoan" %>


<%@ Register Src="~/Controls/Slider.ascx" TagName="Slider" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ContactForm.ascx" TagName="ContactForm" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <section class="module module-divider-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center">
                        <h2>Pożyczka zabezpieczona</h2>
                        <p class="font-serif">
                            <%--Pożyczka zabezpieczona to idealne rozwiązanie.--%>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="icon-box text-center">
                        <div class="icon-box-icon"><span class="fa fa-home"></span></div>
                        <div class="icon-box-title">
                            <h6>Domem lub mieszkaniem</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam lorem ipsum dolor sit amet.</p>--%>
                        </div>
                        <%--<div class="icon-box-link"><a href="#">Take a tour</a></div>--%>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="icon-box text-center">
                        <div class="icon-box-icon"><span class="fa fa-credit-card"></span></div>
                        <div class="icon-box-title">
                            <h6>Wekslem</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam lorem ipsum dolor sit amet.</p>--%>
                        </div>
                        <%--<div class="icon-box-link"><a href="#">Take a tour</a></div>--%>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="icon-box text-center">
                        <div class="icon-box-icon"><i class="fa fa-car"></i></div>
                        <div class="icon-box-title">
                            <h6>Samochodem</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam lorem ipsum dolor sit amet.</p>--%>
                        </div>
                        <%--<div class="icon-box-link"><a href="#">Take a tour</a></div>--%>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="icon-box text-center">
                        <div class="icon-box-icon"><span class="fa fa-connectdevelop"></span></div>
                        <div class="icon-box-title">
                            <h6>Dziełem sztuki</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Lorem ipsum dolor sit amet consectetuer adipiscing elit sed diam lorem ipsum dolor sit amet.</p>--%>
                        </div>
                        <%--<div class="icon-box-link"><a href="#">Take a tour</a></div>--%>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="m-b-100"></div>
                </div>
            </div>
            <uc:Slider ID="SliderControl" runat="server" />
            <div style="margin-top: 50px">
                <h3 style="text-align: center">Wypełnij formularz </h3>
               <uc1:ContactForm ID="ContactFormControl" runat="server" />
            </div>
        </div>

    </section>

</asp:Content>