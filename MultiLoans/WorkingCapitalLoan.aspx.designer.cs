﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace MultiLoans {
    
    
    public partial class WorkingCapitalLoan {
        
        /// <summary>
        /// SliderControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MultiLoans.Controls.Slider SliderControl;
        
        /// <summary>
        /// ContactFormControl control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::MultiLoans.ContactForm ContactFormControl;
    }
}
