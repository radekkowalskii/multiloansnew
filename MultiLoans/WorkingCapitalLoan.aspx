﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WorkingCapitalLoan.aspx.cs" Inherits="MultiLoans.WorkingCapitalLoan" %>

<%@ Register Src="~/Controls/ContactForm.ascx" TagName="ContactForm" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Slider.ascx" TagName="Slider" TagPrefix="uc" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">


    <section class="module module-divider-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center">
                        <h2>Pożyczka obrotowa</h2>
                        <%--<p class="font-serif">Idealne rozwiązanie zabezpieczone nieruchomością.</p>--%>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-4">
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-webpage-multiple"></span></div>
                        <div class="icon-box-title">
                            <h6>Przejrzyste zasady</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Map where your photos were taken and discover local points.</p>--%>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-hammer"></span></div>
                        <div class="icon-box-title">
                            <h6>Dopasowane do Twoich potrzeb</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Map where your photos were taken and discover local points.</p>--%>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon-basic-paperplane"></span></div>
                        <div class="icon-box-title">
                            <h6>Dowolny cel</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Map where your photos were taken and discover local points.</p>--%>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class=" icon-basic-bolt"></span></div>
                        <div class="icon-box-title">
                            <h6>Szybka decyzja pożyczkowa</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Map where your photos were taken and discover local points.</p>--%>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <p></p>
                    <p class="text-center">
                        <img src="assets/images/laptop.jpg" alt="">
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="module module-divider-bottom">
        <div class="container">
           <%-- <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center">
                        <h2>Główne zalety produktu</h2>
                        <p class="font-serif">Lorem ipsum lorem ipsum.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="icon-box">
                        <div class="icon-box-icon" data-color="#e75710"><span class="icon icon-basic-gear"></span></div>
                        <div class="icon-box-title">
                            <h6>Fully Responsive</h6>
                        </div>
                        <div class="icon-box-content">
                            <p>Map where your photos were taken and discover local points of interest. Map where your photos were taken and discover local points of interest.</p>
                        </div>
                        <div class="icon-box-link"><a href="#" data-color="#888888">Take a tour &rarr;</a></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box">
                        <div class="icon-box-icon" data-color="#cd669a"><span class="icon icon-basic-globe"></span></div>
                        <div class="icon-box-title">
                            <h6>Unlimited Colors</h6>
                        </div>
                        <div class="icon-box-content">
                            <p>Map where your photos were taken and discover local points of interest. Map where your photos were taken and discover local points of interest.</p>
                        </div>
                        <div class="icon-box-link"><a href="#" data-color="#888888">Take a tour &rarr;</a></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box">
                        <div class="icon-box-icon" data-color="#f39c11"><span class="icon icon-basic-spread-text"></span></div>
                        <div class="icon-box-title">
                            <h6>Google Web Fonts</h6>
                        </div>
                        <div class="icon-box-content">
                            <p>Map where your photos were taken and discover local points of interest. Map where your photos were taken and discover local points of interest.</p>
                        </div>
                        <div class="icon-box-link"><a href="#" data-color="#888888">Take a tour &rarr;</a></div>
                    </div>
                </div>
            </div>--%>


            <uc:Slider ID="SliderControl" runat="server"></uc:Slider>

            <div style="margin-top: 50px">
                <h3 style="text-align: center">Wypełnij formularz </h3>
                <uc1:ContactForm ID="ContactFormControl" runat="server" />
            </div>
        </div>
    </section>
</asp:Content>
