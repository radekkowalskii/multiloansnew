


    $(function () {
        $("#slider").slider(
            {
                value: 10000,
                min: 10000,
                max: 500000,
                step: 1000,
                slide: function (event, ui) {
                    var val = $('#slider').slider("option", "value");
                    $('#txbSliderValue').val(val)
                },
                change: function (event, ui) {
                    var val = $('#slider').slider("option", "value");
                    $('#txbSliderValue').val(val)
                }
            }
        );

        $('#txbSliderValue').keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                var value = this.value.substring(0);
                $("#slider").slider("value", value);
                return false;
            }
        });

        $('#txbSliderValue').change(function (event) {
            var value = this.value.substring(0);
            $("#slider").slider("value", value);
        });

        /*slider cash return time period*/
        $("#slider1").slider(
            {
                value: 12,
                min: 6,
                max: 36,
                step: 1,
                slide: function (event, ui) {
                    var val = $('#slider1').slider("option", "value");
                    $('#txbSliderValue1').val(val)
                    ManageMonthsText(val);
                },
                change: function (event, ui) {
                    var val = $('#slider1').slider("option", "value");
                    $('#txbSliderValue1').val(val)
                    ManageMonthsText(val);
                }
            }
        );

        $('#txbSliderValue1').keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                var value = this.value.substring(0);
                $("#slider1").slider("value", value);
                ManageMonthsText(value);
                return false;
            }
        });

        $('#txbSliderValue1').change(function (event) {
            var value = this.value.substring(0);
            $("#slider1").slider("value", value);
            ManageMonthsText(value);
        });
    });

    function ManageMonthsText(value) {
        var array = [22, 23, 24, 32, 33, 34];

        if (array.indexOf(parseInt(value)) != -1) {
            $("#lblCashReturnTimePeriod").text("miesi�ce");
        }
        else {
            $("#lblCashReturnTimePeriod").text("miesi�cy");
        }
    }

