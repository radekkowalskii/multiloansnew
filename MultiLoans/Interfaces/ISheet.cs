﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiLoans.Interfaces
{
    public interface ISheet
    {
        byte[] GenerateSheet();
        string GetReportFileName();
    }

    public enum SheetsTypes
    {
        UniSheet,
        ContactSheet
    }
}
