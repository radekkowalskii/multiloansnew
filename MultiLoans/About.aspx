﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="MultiLoans.About" %>

<%@ Register Src="~/Controls/ContactForm.ascx" TagName="ContactForm" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <meta name="robots" content="noindex, nofollow" />
   <section class="module module-divider-bottom" >
        <div class="container" style="margin-top:50px">
            <div class="row">
                <div class="col-md-12 m-auto">
                    <div class="module-title text-center">
                        <h2>MULTILOANS</h2>
                    </div>
                    <div class="module-title text-center">
                        MutliLoans to platforma ułatwiająca małym, średnim oraz spółkom kapitałowym dostęp do finansowania
zewnętrznego. Odmówiono Ci finansowania?<br /> Skorzystaj z naszej platformy i uzyskaj informacje na
temat produktu, który Cię interesuje.
         
            
                Dzięki Nam nie musisz wypełniać setek formularzy czy zapytań, kontaktować się telefonicznie z infolinią
lub szukać stacjonarnych oddziałów. W jednym miejscu znajdziesz cały rynek prywatnego wsparcia
pożyczkowego dla biznesu.</div>
           <div class="module-title text-center">
                MultiLoans powstał jako portal wspierający biznes w pozyskiwaniu optymalnych rozwiązań finansowych
oraz mający na celu ochronę interesu naszego klienta przed lichwiarskimi i nieuczciwymi firmami
pożyczkowymi. Z tego powodu podmioty, których produkty są dostępne w naszym asortymencie są
członkami Konferencji Przedsiębiorstw Finansowych w Polsce lub Związku Faktorów Polskich. Nie
jesteśmy firmą pożyczkową, lecz bezpłatnym narzędziem ułatwiającym Tobie dostęp do finansowania.
Nasza platforma charakteryzuje się transparentnością oraz działaniem w myśl społeczno–etycznej
odpowiedzialności w biznesie.</div>
            <div class="module-title text-center">
                MultiLoans nie pobiera od klienta żadnych prowizji ani opłat wynikających z faktu udzielenia
finansowania za pośrednictwem Naszej platformy.
                    </div>
                    <div class="module-title text-center">
                        <h3>Masz pytania?</h3>
                        <p class="font-serif">Wyślij do nas wiadomość, skontaktujemy się z Tobą!</p>
                    </div>
                    <uc:ContactForm ID="ContactForm" runat="server"></uc:ContactForm>
                </div>
            </div>
        </div>
    </section>
</asp:Content>
