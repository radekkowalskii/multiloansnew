﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CarSecuredLoan.aspx.cs" Inherits="MultiLoans.CarSecuredLoan" %>

<%@ Register Src="~/Controls/ContactForm.ascx" TagName="ContactForm" TagPrefix="uc1" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .arrow-list > li:before {
            font: normal normal normal 14px/1 FontAwesome;
            speak: none;
            font-style: normal;
            font-weight: 400;
            font-variant: normal;
            text-transform: none;
            -webkit-font-smoothing: antialiased;
            content: "\f105";
            font-size: 12px;
            display: inline-block;
            margin-right: 18px;
            position: relative;
            top: 0;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            ShowCarAdditionalFields();
        });
    </script>
    <section class="module">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center">
                        <h2>Leasing &<br />
                            pożyczka zabezpieczona autem</h2>
                        <%--<p class="font-serif">Idealne rozwiązanie zabezpieczone nieruchomością.</p>--%>
                    </div>
                </div>
            </div>
            <div class="row m-b-40">
                <div class="col-md-4">
                    <div class="special-heading">
                        <h4>Kredytobiorca</h4>
                    </div>
                    <ul class="arrow-list">
                        <li>Wiek kredytobiorcy od 21 do 70 lat</li>
                        <li>Osoby fizyczne</li>
                        <li>Osoby fizyczne prowadzące działalność</li>
                        <li>Tylko jedna osoba w kredycie<br />
                            (tylko 1 osoba w dowodzie rej.)</li>
                        <li>Również osoby z wpisami do BIK i KRD</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="special-heading">
                        <h4>Pojazd</h4>
                    </div>
                    <ul class="arrow-list">
                        <li>Samochody do 3,5t</li>
                        <li>Wiek pojazdu bez ograniczeń</li>
                        <li>Z wyłączeniem pojazdów specjalistycznych
                            <br />
                            (np. TAXI; L; ambulans, itp.)</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="special-heading">
                        <h4>Zabezpieczenia</h4>
                    </div>
                    <ul class="arrow-list">
                        <li>Przewłaszczenie częściowe (51/49)</li>
                        <li>Brak AC, wymagane jest aktualne OC</li>
                    </ul>
                </div>
            </div>
            <div class="row m-b-40">
                <div class="col-md-4">
                    <div class="special-heading">
                        <h4>Kwota kredytu</h4>
                    </div>
                    <ul class="arrow-list">
                        <li>Min. 2.500 PLN, max. 45.000 PLN</li>
                        <li>Max. 85% wartości pojazdu</li>
                        <li>Wartość pojazdu ustalana na podstawie „OTOMOTO” i „Gratki”</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="special-heading">
                        <h4>Procedura</h4>
                    </div>
                    <ul class="arrow-list">
                        <li>Wyciągi z konta za 3 ostatnie miesiące lub inne akceptowane przez Mogo dokumenty finansowe</li>
                        <li>Rata kredytu w Mogo nie może przekroczyć 40% miesięczne dochodu kredytobiorcy (do zdolności nie są liczone inne zobowiązania klienta)</li>
                        <li>Możliwość  finansowania 60% wartości pojazdu bez pokazywania dokumentów finansowych”</li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="special-heading">
                        <h4>Umowa pożyczki</h4>
                    </div>
                    <ul class="arrow-list">
                        <li>Prosta, przejrzysta i zgodna z Ustawą o kredycie konsumenckim</li>
                        <li>Wszystkie koszty czytelnie ujęte w harmonogramie (prowizja rozbita na raty)</li>
                        <li>Nominalnie łączny koszt kredytu wynosi między 20-25% w skali roku</li>
                        <li>Przy wcześniejszej spłacie klient spłaca sam kapitał (bez pozostałych odsetek i <b>prowizji</b>)</li>
                        <li>Brak dodatkowych ukrytych kosztów</li>
                    </ul>
                </div>
            </div>
            <div style="margin-top: 50px">
                <h3 style="text-align: center">Wypełnij formularz </h3>
                <uc1:ContactForm ID="ContactFormControl" runat="server" />
            </div>
        </div>
    </section>
</asp:Content>
