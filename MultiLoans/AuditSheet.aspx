﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuditSheet.aspx.cs" Inherits="MultiLoans.AuditSheet" %>

<%@ Register Src="~/Controls/AuditSheetControl.ascx" TagName="AuditControl" TagPrefix="uc" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container" style="margin-top: 100px; margin-bottom:140px">
        <uc:AuditControl runat="server"></uc:AuditControl>
    </div>
</asp:Content>
