﻿using MultiLoans;
using MultiLoans.App_Code;
using MultiLoans.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MultiLoans.Sheets
{
    public class UniSheetClass : ISheet
    {
        const string FILE_NAME = "UniSheet.rdlc";

        public byte[] GenerateSheet()
        {
            ReportsGenerator reportGenerator = new ReportsGenerator();
            return reportGenerator.PrintSheet(this);
        }

        public SheetsTypes GetSheetType()
        {
            return SheetsTypes.UniSheet;
        }

        public string GetReportFileName()
        {
            //aa1.mytableDataTable dt = new aa1.mytableDataTable();
            //var xy = dt.Select(x => x.firstname.Equals("")).ToList();
            //dt.AcceptChanges();

            //foreach (var item in xy)
            //{

            //}
            return FILE_NAME;
        }
        
    }
}