﻿using Microsoft.Reporting.WebForms;
using MultiLoans.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using Npgsql;
using System.Web.Configuration;

namespace MultiLoans.App_Code
{
    public class ReportsGenerator
    {
        internal byte[] PrintSheet(ISheet sheet)
        {
            //string report_name = sheet.GetReportFileName();
            string report_name = "Report2.rdlc";
            Warning[] warnings;
            string[] streamids;
            string mimeType, encoding, extension = string.Empty;
            string deviceInfo = @"
                  <DeviceInfo>
                    <OutputFormat>PDF</OutputFormat>
                    <PageWidth>8.5in</PageWidth>
                    <PageHeight>11in</PageHeight>
                    <MarginTop>0.25in</MarginTop>
                    <MarginLeft>0.25in</MarginLeft>
                    <MarginRight>0.25in</MarginRight>
                    <MarginBottom>0.25in</MarginBottom>
                  </DeviceInfo>";

            LocalReport report = new LocalReport();

            report.DataSources.Clear();
            report.EnableExternalImages = true;

            report.ReportPath = string.Format("{0}{1}\\{2}", HttpContext.Current.Request.PhysicalApplicationPath, "App_Data", report_name);

            DataTable dt = new DataTable();

            //DataColumn col = new DataColumn("firstname", typeof(String));
            //dt.Columns.Add(col);

            //col = new DataColumn("surname", typeof(string));
            //dt.Columns.Add(col);

            //DataRow row = dt.NewRow();
            //row[0] = "Radek";
            //row[1] = "Kowalski";

            //DataRow row1 = dt.NewRow();
            //row1[0] = "Jakub";
            //row1[1] = "Bańka";

            //dt.Rows.Add(row);
            //dt.Rows.Add(row1);

            using (NpgsqlConnection conn = new NpgsqlConnection(WebConfigurationManager.ConnectionStrings["multiloansConnectionString"].ConnectionString))
            {
                conn.Open();
                using (NpgsqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT * FROM unisheets";

                    using (NpgsqlDataAdapter da = new NpgsqlDataAdapter(cmd))
                    {
                        da.Fill(dt);
                    }
                }
            }
            report.DataSources.Add(new ReportDataSource("DataSet1", (DataTable)dt));
            
            return report.Render("PDF", deviceInfo, out mimeType,
                out encoding, out extension, out streamids, out warnings);
        }
        
    }
}