﻿using MultiLoansLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MultiLoans.Controls
{
    public partial class AuditSheetControl : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        //public string FirstName { get { return txbFirstName.Text  } }

        public MultiLoansLib.Models.AuditSheet GetAuditSheetData()
        {
            MultiLoansLib.Models.AuditSheet auditSheet = new MultiLoansLib.Models.AuditSheet
            {
                Firstname = txbFirstname.Text,
                Surname = txbSurname.Text,
                Email = txbEmail.Text,
                Phone = txbPhone.Text,
                Nip = txbNip.Text,
                TwoYearsSales = Convert.ToDouble(txbTwoYearsIncome.Text),
                TwoYearsNetProfit = Convert.ToDouble(txbTwoYearsIncomeNoTax.Text),
                CreditsCount = Convert.ToInt32(txbCreditNumber.Text),
                MonthlyCreditInstallment = Convert.ToInt32(txbCreditMonthlyInstallment.Text),
                CompanyType = Convert.ToInt32(rdbtnCompanyType.SelectedItem.Value)
            };
           
            return auditSheet;
        }
    }
}