﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AuditSheetControl.ascx.cs" Inherits="MultiLoans.Controls.AuditSheetControlNew" %>

<style>
    .table thead > tr > th, .table tbody > tr > th, .table tfoot > tr > th, .table thead > tr > td, .table tbody > tr > td, .table tfoot > tr > td {
        border-top: none !important;
    }

    .centralDiv {
        margin-bottom: 20px;
        margin-top: 50px;
    }
</style>
<%--<div class="centralDiv">
        <div class="row">
            <div class="col-12">
                <h1 style="text-align: center; text-transform: uppercase; font-weight: bold">WNIOSEK AUDYTOWY
                </h1>
            </div>
        </div>
    </div>--%>
<div class="container">
    <div class="table-responsive">
        <table class="table">
                <tr>
                    <td style="width: 30%"></td>
                    <td style="width: 70%"></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFirstname" runat="server" Text="Imię"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txbFirstname" runat="server" CssClass="form-control" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblSurname" runat="server" Text="Nazwisko"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txbSurname" runat="server" CssClass="form-control" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblEmail" runat="server" Text="Adres email"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txbEmail" runat="server" TextMode="Email" CssClass="form-control" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPhone" runat="server" Text="Numer telefonu"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txbPhone" runat="server" TextMode="Phone" CssClass="form-control" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblNip" runat="server" Text="Numer NIP"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txbNip" runat="server" CssClass="form-control" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:RadioButtonList ID="rdbtnCompanyType" runat="server" RepeatDirection="Horizontal"
                            RepeatLayout="Table">
                            <asp:ListItem Enabled="true" Selected="True" Text="Działalność gospodarcza" Value="1">
                            </asp:ListItem>
                            <asp:ListItem Enabled="true" Selected="False" Text="Spółka" Value="2">
                            </asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblTwoYearsIncome" runat="server" Text="Obroty za ostatnie 2 lata obrachunkowe"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txbTwoYearsIncome" runat="server" TextMode="Number" CssClass="form-control" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblTwoYearsIncomeNoTax" runat="server" Text="Zysk netto za ostatnie 2 lata obrachunkowe"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txbTwoYearsIncomeNoTax" runat="server" TextMode="Number" CssClass="form-control" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblCreditNumber" runat="server" Text="Liczba zaciągniętych kredytów"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txbCreditNumber" runat="server" TextMode="Number" CssClass="form-control" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblCreditMonthlyInstallment" runat="server" Text="Łączna miesięczna rata kredytu"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txbCreditMonthlyInstallment" runat="server" TextMode="Number" CssClass="form-control" Width="100%"></asp:TextBox></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:CheckBoxList ID="cbxListAcceptances" runat="server">
                            <asp:ListItem Text="Wyrażam zgodę na przetwarzanie danych osobowych" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Wyrażam zgodę na otrzymywanie informacji marketingowych" Value="1"></asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <%--<tr>
                    <td colspan="2" style="text-align:center">
                        <asp:Button ID="btnSendAudit" runat="server" Text="Wyślij" CssClass="btn btn-primary" Width="160px"/>
                    </td>
                </tr>--%>
        </table>
    </div>
    
</div>
