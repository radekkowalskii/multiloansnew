﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContactForm.ascx.cs" Inherits="MultiLoans.ContactForm" %>

<script type="text/javascript">
    function ShowCarAdditionalFields() {
        console.log("loaded");
            $("#divCarAdditionalFields").show();
        }
    </script><style>
@media screen and (max-width: 768px){
    #btnSendContactRequest{
        width:100%;
    }
    }
        </style>
<div class="container">
    <div class="row m-b-50">
        <div class="col-md-8 m-auto">

            <div class="form-group row">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-text-input">Imię</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbFirstname" runat="server" CssClass="form-control">
                    </asp:TextBox>
                </div>
            </div>

            <div class="form-group row">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-text-input">Nazwisko</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbSurname" runat="server" CssClass="form-control">
                    </asp:TextBox>
                </div>
            </div>

            <div class="form-group row">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-text-input">Nazwa firmy</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbCompanyName" runat="server" CssClass="form-control">
                    </asp:TextBox>
                </div>
            </div>

            <div class="form-group row">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-number-input">NIP</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbNip" runat="server" CssClass="form-control" TextMode="Number">
                    </asp:TextBox>
                </div>
            </div>

            <div class="form-group row" id="sa" runat="server">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-email-input">Email</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbEmail" runat="server" CssClass="form-control" TextMode="Email">
                        
                    </asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" ValidationGroup="testValidationGroup" ErrorMessage="Podaj adres email" ControlToValidate="txbEmail">

                    </asp:RequiredFieldValidator>
                </div>
            </div>

            <div class="form-group row">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-tel-input">Telefon</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbPhone" runat="server" CssClass="form-control" TextMode="Phone">
                    </asp:TextBox>
                </div>
            </div>

            <div id="divCarAdditionalFields" style="display:none">
                <div class="form-group row">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-tel-input">Numer nadwozia</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbCarBodyNumber" runat="server" CssClass="form-control" TextMode="Number">
                    </asp:TextBox>
                </div>
            </div>
                <div class="form-group row">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-tel-input">Numer rejestracyjny</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbCarRegistrationNumber" runat="server" CssClass="form-control" TextMode="Number">
                    </asp:TextBox>
                </div>
            </div>
                <div class="form-group row">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-tel-input">Data pierwszej rejestracji</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbFirstRegistrationDate" runat="server" CssClass="form-control" TextMode="Date">
                    </asp:TextBox>
                </div>
            </div>
                <div class="form-group row">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-tel-input">Cena auta</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbCarPrice" runat="server" CssClass="form-control" TextMode="Number">
                    </asp:TextBox>
                </div>
            </div>
                 <div class="form-group row">
                <asp:Label runat="server" class="col-2 col-form-label" for="example-tel-input">Wpłata własna</asp:Label>
                <div class="col-10">
                    <asp:TextBox ID="txbSelfDeposit" runat="server" CssClass="form-control" TextMode="Number">
                    </asp:TextBox>
                </div>
            </div>
            </div>

        </div>
    </div>
    <div class="row m-t-50" style="margin-bottom: 50px;">
        <div class="col-md-12">
            <div class="text-center">
                <asp:Button runat="server" ID="btnSendContactRequest" ClientIDMode="Static"  CssClass="btn btn-primary btn-md" Text="Wyślij" 
                    OnClick="btnSendContactRequest_Click" ValidationGroup="testValidationGroup" />
            </div>
        </div>
    </div>

</div>
