﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MultiLoans.Controls
{
    public partial class AuditSheetUC : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private MultiLoansLib.Models.AuditSheet GetAuditSheetData()
        {
            MultiLoansLib.Models.AuditSheet auditSheet = new MultiLoansLib.Models.AuditSheet
            {
                Firstname = txbFirstname.Text,
                Surname = txbSurname.Text,
                Email = txbEmail.Text,
                Phone = txbPhone.Text,
                Nip = txbNip.Text,
                TwoYearsSales = Convert.ToDouble(txbTwoYearsIncome.Text),
                TwoYearsNetProfit = Convert.ToDouble(txbTwoYearsIncomeNoTax.Text),
                CreditsCount = Convert.ToInt32(txbCreditNumber.Text),
                MonthlyCreditInstallment = Convert.ToInt32(txbCreditMonthlyInstallment.Text),
                CompanyType = Convert.ToInt32(rdbtnCompanyType.SelectedItem.Value)
            };

            return auditSheet;
        }

        protected void btnSendAudit_Click(object sender, EventArgs e)
        {
            var auditData = GetAuditSheetData();
            MultiLoansLib.Models.AuditSheet auditSheet = new MultiLoansLib.Models.AuditSheet();
            auditSheet.Firstname = txbFirstname.Text;
            auditSheet.Surname = txbSurname.Text;
            auditSheet.Email = txbEmail.Text;
            auditSheet.Phone = txbPhone.Text;
            auditSheet.Nip = txbNip.Text;
            auditSheet.TwoYearsSales = Convert.ToDouble(txbTwoYearsIncome.Text);
            auditSheet.TwoYearsNetProfit = Convert.ToDouble(txbTwoYearsIncomeNoTax.Text);
            auditSheet.CreditsCount = Convert.ToInt32(txbCreditNumber.Text);
            auditSheet.MonthlyCreditInstallment = Convert.ToInt32(txbCreditMonthlyInstallment.Text);
            auditSheet.CompanyType = Convert.ToInt32(rdbtnCompanyType.SelectedItem.Value);

            auditSheet.Insert();
            //auditSheet.InsertAsync();
            
        }
    }
}