﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FactoringSheet.ascx.cs" Inherits="MultiLoans.Controls.FactoringSheet" %>


<div class="row" style="margin:5px; padding-top:5px; ">
    <div class="col-lg-offset-2 col-lg-3 col-md-2 col-sm-12 col-2" style="padding-top:5px; text-align: left; color: #486075;">
         <span class="glyphicon glyphicon-user"></span>
        <asp:Label ID="lblPersonDetails" runat="server" Text="Imię i nazwisko"></asp:Label>
    </div>
    <div class="col-lg-4 col-md-3 col-sm-12 col-3" style="padding-top: 10px;">
       <asp:TextBox ID="txbPersonDetails" runat="server" CssClass="txb-FactoringSheet-form-control" Width="100%"></asp:TextBox>
    </div>
</div>
<div class="row" style="margin:5px; padding-top:5px; ">
    <div class="col-lg-offset-2 col-lg-3 col-md-2 col-sm-12 col-2" style="padding-top:5px; text-align: left; color: #486075;">
        <span class="glyphicon glyphicon-envelope"></span>
                        <asp:Label ID="lblEmail" runat="server" Text="Nazwa firmy"></asp:Label>
    </div>
    <div class="col-lg-4 col-md-3 col-sm-12 col-3" style="padding-top: 10px;">
       <asp:TextBox ID="txbEmail" runat="server" CssClass="txb-FactoringSheet-form-control" Width="100%"></asp:TextBox>
    </div>
</div>
<div class="row" style="margin:5px; padding-top:5px; ">
    <div class="col-lg-offset-2 col-lg-3 col-md-2 col-sm-12 col-2" style="padding-top:5px; text-align: left; color: #486075;">
         <span class="glyphicon glyphicon-pencil"></span>
                         <asp:Label ID="lblContent" runat="server" Text="NIP"></asp:Label>
    </div>
    <div class="col-lg-4 col-md-3 col-sm-12 col-3" style="padding-top: 10px;">
       <asp:TextBox ID="txbContent" Width="100%" CssClass="txb-FactoringSheet-form-control" runat="server"></asp:TextBox>
    </div>
</div>
<div class="row" style="margin:5px; padding-top:5px; ">
    <div class="col-lg-offset-2 col-lg-3 col-md-2 col-sm-12 col-2" style="padding-top:5px; text-align: left; color: #486075;">
         <span class="glyphicon glyphicon-phone"></span>
                        <asp:Label ID="lblPhone" runat="server" Text="Telefon"></asp:Label>
    </div>
    <div class="col-lg-4 col-md-3 col-sm-12 col-3" style="padding-top: 10px;">
       <asp:TextBox ID="txbPhone" runat="server" TextMode="Phone" CssClass="txb-FactoringSheet-form-control" Width="100%"></asp:TextBox>
    </div>
</div>

<div class="row" style="margin:5px; padding-top:5px; text-align:center">
    <div class="col-lg-4 col-lg-offset-4 col-md-3 col-md-offset-1 col-sm-12 col-sm-offset- col-3 col-offset-1" style="text-align:center">
        <asp:Button runat="server" ID="btnSendContactRequest" Width="70%" CssClass="btn btn-primary btn-md" Text="Wyślij" />
    </div>
</div>