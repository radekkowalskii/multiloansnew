﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Slider.ascx.cs" Inherits="MultiLoans.Controls.Slider" %>



<script>


    $(function () {
        $("#slider").slider(
            {
                value: 10000,
                min: 10000,
                max: 500000,
                step: 1000,
                slide: function (event, ui) {
                    var val = $('#slider').slider("option", "value");
                    $('#txbSliderValue').val(val)
                },
                change: function (event, ui) {
                    var val = $('#slider').slider("option", "value");
                    $('#txbSliderValue').val(val)
                }
            }
        );

        $('#txbSliderValue').keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                var value = this.value.substring(0);
                $("#slider").slider("value", value);
                return false;
            }
        });

        $('#txbSliderValue').change(function (event) {
            var value = this.value.substring(0);
            $("#slider").slider("value", value);
        });

        /*slider cash return time period*/
        $("#slider1").slider(
            {
                value: 12,
                min: 6,
                max: 36,
                step: 1,
                slide: function (event, ui) {
                    var val = $('#slider1').slider("option", "value");
                    $('#txbSliderValue1').val(val)
                    ManageMonthsText(val);
                },
                change: function (event, ui) {
                    var val = $('#slider1').slider("option", "value");
                    $('#txbSliderValue1').val(val)
                    ManageMonthsText(val);
                }
            }
        );

        $('#txbSliderValue1').keydown(function (event) {
            if (event.keyCode == 13) {
                event.preventDefault();
                var value = this.value.substring(0);
                $("#slider1").slider("value", value);
                ManageMonthsText(value);
                return false;
            }
        });

        $('#txbSliderValue1').change(function (event) {
            var value = this.value.substring(0);
            $("#slider1").slider("value", value);
            ManageMonthsText(value);
        });
    });

    function ManageMonthsText(value) {
        var array = [22, 23, 24, 32, 33, 34];

        if (array.indexOf(parseInt(value)) != -1) {
            $("#lblCashReturnTimePeriod").text("miesiące");
        }
        else {
            $("#lblCashReturnTimePeriod").text("miesięcy");
        }
    }


</script>
<style>
    .ui-widget.ui-widget-content {
        border: 1.5px solid transparent;
        background-color: #ffe9ab;
        box-shadow: 4px 5px 14px 0px rgba(253, 186, 3, 0.5);
    }

    .ui-slider .ui-slider-handle {
        width: 25px;
        height: 23px;
        left: -.6em;
        text-decoration: none;
        text-align: center;
        background-color: #fdc11c;
        border-color: #fdc11c;
        border-radius: 50%;
        outline: 0 !important;
        
    }

    .txbAmmount{
        border:none !important;
        text-align:center !important;
    }
</style>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="page-header">
            <h3 style="text-align: center">Wybierz kwotę</h3>
        </div>
        <div style="margin-top: 30px; " class="ui-slider ui-slider-handle" id="slider"></div>
        <div style="width: 50%; margin: 0 auto; margin-top: 20px; text-align: center ;text-align:center !important; border:none !important">
            <asp:TextBox AutoPostBack="false" ID="txbSliderValue" Text="10000" runat="server" AutoCompleteType="Disabled"
                ClientIDMode="Static" CssClass="txbAmmount">
            </asp:TextBox>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0" style="margin-top: 40px">
        <div class="page-header">
            <h3 style="text-align: center">Wybierz okres spłaty</h3>
        </div>
        <div style="margin-top: 30px; t" class="ui-slider ui-slider-handle" id="slider1"></div>
        <div style="width: 100%; margin: 0 auto; margin-top: 20px; text-align: center">
            <div class="col-md-5 col-sm-3 col-xs-5" style="padding: 0px; margin: 0px;text-align:center !important; border:none !important">
                <asp:TextBox AutoPostBack="false"  ID="txbSliderValue1" Text="12" runat="server" AutoCompleteType="Disabled"
                    ClientIDMode="Static" CssClass="txbAmmount">
                </asp:TextBox>
            </div>
            <div class="col-md-7 col-sm-7 col-xs-7" style="padding: 0px; margin: 0px;">
                <asp:Label ID="lblCashReturnTimePeriod" ClientIDMode="Static" runat="server" Text="miesięcy" CssClass="tbx-securedLoansCash-form-control2"></asp:Label>
            </div>
        </div>
    </div>
</div>
