﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MultiLoans.Default" %>

<%@ Register Src="~/Controls/AuditSheetUC.ascx" TagName="AuditControl" TagPrefix="uc" %>
<%@ Register Src="~/Controls/ContactForm.ascx" TagName="ContactControl" TagPrefix="uc" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .modal-dialog .modal-lg .ui-dialog-titlebar {
            display: none
        }
    </style>
    <script>
        $(function () {
            $('#btnPopupAuditSheet').click(function (e) {
                e.preventDefault();
            });


            var dlg = $("#dialog").dialog({
                autoOpen: false,
                draggable: true,
                fluid: true,
                height: 'auto',

                maxHeight: 700,
                minWidth: 600,
                maxWidth: 900,
                modal: true,
                resizable: false,
                dialogClass: 'dlgfixed',
                position: {
                    my: "center",
                    at: "center",
                    of: window,
                    collision: "none"
                },
                open: function (event, ui) {
                    $('.ui-widget-overlay').bind('click', function () {
                        $("#dialog").dialog('close');
                    });
                }
            });

            dlg.parent().appendTo(jQuery("form:first"));

            $("#btnPopupAuditSheet").on("click", function () {
                $("#dialog").dialog("open");
            });

            $(".ui-dialog-titlebar").hide();

        });

        //$(document).on("dialogopen", ".ui-dialog", function (event, ui) {
        //    fluidDialog();
        //});

        //function fluidDialog() {
        //    var $visible = $(".ui-dialog:visible");
        //    // each open dialog
        //    $visible.each(function () {
        //        var $this = $(this);
        //        var dialog = $this.find(".ui-dialog-content").data("ui-dialog");
        //        // if fluid option == true
        //        if (dialog.options.fluid) {
        //            var wWidth = $(window).width();
        //            // check window width against dialog width
        //            if (wWidth < (parseInt(dialog.options.maxWidth) + 50)) {
        //                // keep dialog from filling entire screen
        //                $this.css("max-width", "90%");
        //            } else {
        //                // fix maxWidth bug
        //                $this.css("max-width", dialog.options.maxWidth + "px");
        //            }
        //            //reposition dialog
        //            dialog.option("position", dialog.options.position);
        //        }
        //    });

        $(window).resize(function () {
            $("#dialog").dialog("option", "position", { my: "center", at: "center", of: window });
        });

        function OpenDialogBox() {
            $("#dialog").dialog("open");
        }
        function CloseDialogBox() {
            $("#dialog").dialog("close");
        }

    </script>


    <!-- Page Header-->
    <section class="module-header full-height parallax bg-dark bg-dark-60" data-background="img/baner_main.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="h1">Multiloans</h1>
                    <%--<h1 class="h1 m-b-15"></h1>--%>
                    <p class="m-b-30">Pierwszy w Polsce multiportal finansowania prywatnego dla biznesu</p>
                    <p>
                        <asp:Button ID="btnPopupAuditSheet" BackColor="#0e4e98" BorderColor="#0e4e98" runat="server" class="btn btn-lg btn-circle btn-brand"
                            Text="Wniosek pożyczkowy" OnClientClick="OpenDialogBox(); return false;"></asp:Button>
                        <p class="font-serif">
                            Wypełnij wniosek, Weź pożyczkę i rozwijaj firmę.
                        </p>
                        <%--<a class="btn btn-lg btn-outline btn-circle btn-white" href="#">Learn More</a>--%>
                    </p>
                </div>

            </div>
        </div>
    </section>

    <div id="dialog">
        <div>
            <div class="modal-header">
                <h2 class="modal-title">Wniosek audytowy</h2>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close" onclick="CloseDialogBox()">
                    <span>&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <uc:AuditControl ID="AuditSheetCtrl" runat="server" />
            </div>
        </div>
    </div>


    <!-- Modals-->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">


                    <div class="modal fade" id="modal-1">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Modal title</h5>
                                    <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span>&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <iframe width="420" height="250" src="//www.youtube.com/embed/EMy5krGcoOU" frameborder="0" allowfullscreen></iframe>
                                    </p>
                                    <p>Meh synth Schlitz, tempor duis single-origin coffee ea next level ethnic fingerstache fanny pack nostrud. Seitan High Life reprehenderit consectetur cupidatat kogi about me. Photo booth anim 8-bit hella, PBR 3 wolf moon beard Helvetica. Salvia esse nihil, flexitarian Truffaut synth art party deep v chillwave. Seitan High Life reprehenderit consectetur cupidatat kogi.</p>
                                    <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed. Craft beer elit seitan exercitation, photo booth et 8-bit kale chips proident chillwave deep v laborum. Aliquip veniam delectus, Marfa eiusmod Pinterest in do umami readymade swag. Selfies iPhone Kickstarter, drinking vinegar jean shorts fixie consequat flexitarian four loko.</p>
                                </div>
                                <%--<div class="modal-footer">
                                    <button class="btn btn-round btn-gray" type="button">Close</button>
                                    <button class="btn btn-round btn-brand" type="button">Save changes</button>
                                </div>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Modals end-->

     <!-- Portfolio-->
    <section class="module p-b-0">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center">
                        <h2>NASZE PRODUKTY</h2>
                        <p class="font-serif">Idealnie dopasowane do Twoich potrzeb</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Portfolio-->
    <section class="module module-divider-bottom p-0">
        <div class="container-fluid">
            <div class="row row-portfolio-filter">
                <div class="col-md-12">
                    <ul class="filters h5" id="filters">
                        <li><a class="current" href="#" data-filter="*">Wszystkie</a></li>
                        <li><a href="#" data-filter=".loans">Pożyczki</a></li>
                        <li><a href="#" data-filter=".ownProducts">Nasze produkty</a></li>
                    </ul>
                </div>
            </div>
            <div class="row row-portfolio" date-portfolio-type="standard" data-columns="3">
                <div class="grid-sizer"></div>
                <div class="portfolio-item loans undefined">
                    <div class="portfolio-wrapper">
                        <img src="img/pozyczka_zastaw.jpg" alt="">
                        <div class="portfolio-overlay"></div>
                    </div>
                    <div class="portfolio-caption">
                        <h5 class="portfolio-title">Pożyczka zabezpieczona</h5>
                        <div class="portfolio-subtitle font-serif">Loans</div>
                    </div>
                    <a class="portfolio-link" href="SecuredLoan.aspx"></a>
                </div>
                <div class="portfolio-item loans undefined">
                    <div class="portfolio-wrapper">
                        <img src="img/pozyczka_obrotowa.jpg" alt="">
                        <div class="portfolio-overlay"></div>
                    </div>
                    <div class="portfolio-caption">
                        <h5 class="portfolio-title">Pożyczka obrotowa</h5>
                        <div class="portfolio-subtitle font-serif">Loans</div>
                    </div>
                    <a class="portfolio-link" href="WorkingCapitalLoan.aspx"></a>
                </div>
                <div class="portfolio-item ownProducts undefined">
                    <div class="portfolio-wrapper">
                        <img src="img/leasing.jpg" alt="">
                        <div class="portfolio-overlay"></div>
                    </div>
                    <div class="portfolio-caption">
                        <h5 class="portfolio-title">Leasing & Pożyczka zabezpieczona autem</h5>
                        <div class="portfolio-subtitle font-serif">Our Products</div>
                    </div>
                    <a class="portfolio-link" href="CarSecuredLoan.aspx"></a>
                </div>
                <div class="portfolio-item ownProducts undefined">
                    <div class="portfolio-wrapper">
                        <img src="img/finansowanie_spolecznosciowe.jpg" alt="">
                        <div class="portfolio-overlay"></div>
                    </div>
                    <div class="portfolio-caption">
                        <h5 class="portfolio-title">Finansowanie społecznościowe</h5>
                        <div class="portfolio-subtitle font-serif">Our Products</div>
                    </div>
                    <a class="portfolio-link" href="About.aspx"></a>
                </div>
                <div class="portfolio-item loans undefined">
                    <div class="portfolio-wrapper">
                        <img src="img/factoring4.jpg" alt="">
                        <div class="portfolio-overlay"></div>
                    </div>
                    <div class="portfolio-caption">
                        <h5 class="portfolio-title">Faktoring</h5>
                        <div class="portfolio-subtitle font-serif">Loans</div>
                    </div>
                    <a class="portfolio-link" href="Factoring.aspx"></a>
                </div>

                <div class="portfolio-item ownProducts undefined">
                    <div class="portfolio-wrapper">
                        <img src="img/eu.jpg" alt="">
                        <div class="portfolio-overlay"></div>
                    </div>
                    <div class="portfolio-caption">
                        <h5 class="portfolio-title">Pożyczka ze środków Unii Europejskiej</h5>
                        <div class="portfolio-subtitle font-serif">Our Products</div>
                    </div>
                    <a class="portfolio-link" href="About.aspx"></a>
                </div>
            </div>
        </div>
    </section>
    <!-- Portfolio end-->

    <!-- Page Header end-->
    <section class="module module-divider-bottom" style="padding:100px 0px 50px 0px">
        <div class="container">
            <div class="row">
                <div class="col-md-12 m-auto">
                    <div class="module-title text-center">
                        <h2>MULTILOANS</h2>
                    </div>
                    <div class="module-title text-center">
                        MutliLoans to platforma ułatwiająca małym, średnim oraz spółkom kapitałowym dostęp do finansowania
zewnętrznego. Odmówiono Ci finansowania?<br />
                        Skorzystaj z naszej platformy i uzyskaj informacje na
temat produktu, który Cię interesuje.
         
            
                Dzięki Nam nie musisz wypełniać setek formularzy czy zapytań, kontaktować się telefonicznie z infolinią
lub szukać stacjonarnych oddziałów. W jednym miejscu znajdziesz cały rynek prywatnego wsparcia
pożyczkowego dla biznesu.
                    </div>
                    <div class="module-title text-center">
                        MultiLoans powstał jako portal wspierający biznes w pozyskiwaniu optymalnych rozwiązań finansowych
oraz mający na celu ochronę interesu naszego klienta przed lichwiarskimi i nieuczciwymi firmami
pożyczkowymi. Z tego powodu podmioty, których produkty są dostępne w naszym asortymencie są
członkami Konferencji Przedsiębiorstw Finansowych w Polsce lub Związku Faktorów Polskich. Nie
jesteśmy firmą pożyczkową, lecz bezpłatnym narzędziem ułatwiającym Tobie dostęp do finansowania.
Nasza platforma charakteryzuje się transparentnością oraz działaniem w myśl społeczno–etycznej
odpowiedzialności w biznesie.
                    </div>
                    <div class="module-title text-center">
                        MultiLoans nie pobiera od klienta żadnych prowizji ani opłat wynikających z faktu udzielenia
finansowania za pośrednictwem Naszej platformy.
                    </div>
                    <%--<div class="module-title text-center">
                        <h3>Masz pytania?</h3>
                        <p class="font-serif">Wyślij do nas wiadomość, skontaktujemy się z Tobą!</p>
                    </div>--%>
                    <%--<uc:ContactControl ID="ContactForm" runat="server"></uc:ContactControl>--%>
                </div>
            </div>
        </div>
    </section>
    <section class="module p-b-0">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center">
                        <h2>NASZA OFERTA</h2>
                        <p class="font-serif">Proste finansowanie biznesu jest naszą główną zaletą.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Services-->
    <!-- Services-->
    <section class="module module-divider-bottom" style="padding-top: 0px !important; margin-bottom: 100px !important">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-webpage-multiple"></span></div>
                        <div class="icon-box-title">
                            <h6>Transparentność oferty</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-target"></span></div>
                        <div class="icon-box-title">
                            <h6>Troska o uczciwą i przejrzystą komunikację</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-cards-diamonds"></span></div>
                        <div class="icon-box-title">
                            <h6>Szacunek i zaufanie w doradztwie</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-anchor"></span></div>
                        <div class="icon-box-title">
                            <h6>Stosowanie zasad odpowiedzialnej sprzedaży</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-spread-text"></span></div>
                        <div class="icon-box-title">
                            <h6>Własna księga etyki biznesu</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-picture-multiple"></span></div>
                        <div class="icon-box-title">
                            <h6>Pożyczka ze środków unijnych</h6>
                        </div>
                        <div class="icon-box-content">
                            <%--<p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="counter h6">
                        <div class="counter-number">
                            <div class="counter-timer" data-from="0" data-to="75">0</div>
                        </div>
                        <div class="counter-title">Zadowolonych klientów</div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="counter h6">
                        <div class="counter-number">
                            <div class="counter-timer" data-from="0" data-to="110">0</div>
                        </div>
                        <div class="counter-title">Złożonych wniosków</div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="counter h6">
                        <div class="counter-number">
                            <div class="counter-timer" data-from="0" data-to="25">0</div>
                        </div>
                        <div class="counter-title">Poleceń klientów</div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="counter h6">
                        <div class="counter-number">
                            <div class="counter-timer" data-from="0" data-to="23000000">0</div>
                        </div>
                        <div class="counter-title">Udzielonych finansowań</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Counters end-->

    <!-- Notebooks-->
    <section class="module module-gray p-0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <img class="bb" src="img/picture1.png" alt="">
                </div>
            </div>
        </div>
    </section>
    <!-- Notebooks end-->

    <!-- Bars-->

    <section class="module module-gray p-t-0">
        <div class="container" style="padding-top: 100px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="progress-item">
                        <div class="progress-title">Przyznawalność</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-brand" aria-valuenow="60" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span class="pb-number-box"><span class="pb-number"></span>%</span></div>
                        </div>
                    </div>
                    <div class="progress-item">
                        <div class="progress-title">LTV</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-brand" aria-valuenow="80" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span class="pb-number-box"><span class="pb-number"></span>%</span></div>
                        </div>
                    </div>
                    <div class="progress-item">
                        <div class="progress-title">Zadowolonych klientów</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-brand" style="width: 27% !important" role="progressbar" aria-valuemin="0" aria-valuemax="90000"><span class="pb-number-box"><span class="pb-number"></span></span></div>
                        </div>
                    </div>
                </div>
                <%--<div class="col-md-6">
                    <div class="progress-item">
                        <div class="progress-title">Gulp</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-brand" aria-valuenow="60" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span class="pb-number-box"><span class="pb-number"></span>%</span></div>
                        </div>
                    </div>
                    <div class="progress-item">
                        <div class="progress-title">UX Design</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-brand" aria-valuenow="80" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span class="pb-number-box"><span class="pb-number"></span>%</span></div>
                        </div>
                    </div>
                    <div class="progress-item">
                        <div class="progress-title">HTML / CSS3 / SASS</div>
                        <div class="progress">
                            <div class="progress-bar progress-bar-brand" aria-valuenow="50" role="progressbar" aria-valuemin="0" aria-valuemax="100"><span class="pb-number-box"><span class="pb-number"></span>%</span></div>
                        </div>
                    </div>
                </div>--%>
            </div>
        </div>
    </section>
    <!-- Bars end-->
  <%--  <section class="module p-b-0">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center">
                        <%--<h2>NASZE PRODUKTY</h2>
                        <p class="font-serif">Idealnie dopasowane do Twoich potrzeb</p>--%>
                   <%-- </div>
                </div>
            </div>
        </div>
    </section>--%>


<%--    <section>
    </section>--%>

    <!-- Testimonials-->
    <section class="module parallax bg-dark bg-dark-30" data-background="img/quotations_background.jpg">
        <div class="container">
            <div class="row">
                <div class="col-md-6 m-auto">
                    <div class="tms-slides owl-carousel">
                        <div class="tms-item">
                            <div class="tms-icons">
                                <h2><span class="icon icon-basic-message-multiple"></span></h2>
                            </div>
                            <div class="tms-content">
                                <blockquote>
                                    <p>“Pieniądz jest wart tyle, ile warta jest ręka, która go trzyma.”</p>
                                </blockquote>
                            </div>
                            <div class="tms-author"><span>Kard. Roger Etchegaray</span></div>
                        </div>
                        <div class="tms-item">
                            <div class="tms-icons">
                                <h2><span class="icon icon-basic-message-multiple"></span></h2>
                            </div>
                            <div class="tms-content">
                                <blockquote>
                                    <p>“Zachowuj się tak, jakby zasada Twojego działania, była powszechnie obowiązującym prawem.”</p>
                                </blockquote>
                            </div>
                            <div class="tms-author"><span>Richard Bandler</span></div>
                        </div>
                        <div class="tms-item">
                            <div class="tms-icons">
                                <h2><span class="icon icon-basic-message-multiple"></span></h2>
                            </div>
                            <div class="tms-content">
                                <blockquote>
                                    <p>“Przekleństwem biznesu jest stagnacja, biznes to ruch i ciągły rozwój… a elementem napędowym jest mądre gospodarowanie pieniądzem jako ekwiwalentem płatniczym…”</p>
                                </blockquote>
                            </div>
                            <div class="tms-author"><span>Rafał Szyc – multiloans.pl</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonials end-->

    <!-- Clients-->
    <section class="module-sm module-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel clients-carousel" data-carousel-options="{&quot;items&quot;:&quot;4&quot;}">
                        <div class="client">
                            <img src="img/client/iwoca.png" alt="">
                        </div>
                        <div class="client">
                            <img src="img/client/logo1.png" alt="">
                        </div>
                        <div class="client">
                            <img src="img/client/logo2.png" alt="">
                        </div>
                        <div class="client">
                            <img src="img/client/logo1.png" alt="">
                        </div>
                        <div class="client">
                            <img src="img/client/logo2.png" alt="">
                        </div>
                        <div class="client">
                            <img src="img/client/logo1.png" alt="">
                        </div>
                        <div class="client">
                            <img src="img/client/logo2.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Clients end-->

    <%--<!-- Alt Services-->
    <section class="module module-divider-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center">
                        <h2>Alt Services</h2>
                        <p class="font-serif">We provide a complete list of best digital services.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="m-t-30"></div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-webpage-multiple"></span></div>
                        <div class="icon-box-title">
                            <h6>Excellent Designs</h6>
                        </div>
                        <div class="icon-box-content">
                            <p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-target"></span></div>
                        <div class="icon-box-title">
                            <h6>Fully Responsive</h6>
                        </div>
                        <div class="icon-box-content">
                            <p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-cards-diamonds"></span></div>
                        <div class="icon-box-title">
                            <h6>Unlimited Colors</h6>
                        </div>
                        <div class="icon-box-content">
                            <p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 hidden-sm-down">
                    <div class="text-center">
                        <img src="assets/images/iphone.png" alt=""></div>
                </div>
                <div class="col-md-4">
                    <div class="m-t-30"></div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-anchor"></span></div>
                        <div class="icon-box-title">
                            <h6>User Friendly</h6>
                        </div>
                        <div class="icon-box-content">
                            <p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-spread-text"></span></div>
                        <div class="icon-box-title">
                            <h6>Google Web Fonts</h6>
                        </div>
                        <div class="icon-box-content">
                            <p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>
                        </div>
                    </div>
                    <div class="icon-box icon-box-left">
                        <div class="icon-box-icon"><span class="icon icon-basic-picture-multiple"></span></div>
                        <div class="icon-box-title">
                            <h6>Free Updates</h6>
                        </div>
                        <div class="icon-box-content">
                            <p>Especially do at he possession insensible sympathize boisterous it. Songs he on an widen me event truth.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Alt Services end-->--%>

    <!-- News-->
    <%-- <section class="module">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center">
                        <h2>Our News</h2>
                        <p class="font-serif">We share our best ideas in our blog.</p>
                    </div>
                </div>
            </div>
            <div class="row blog-grid">
                <div class="col-md-4 post-item">

                    <!-- Post-->
                    <article class="post">
                        <div class="post-preview">
                            <a href="#">
                                <img src="assets/images/blog/1.jpg" alt=""></a>
                        </div>
                        <div class="post-wrapper">
                            <div class="post-header">
                                <h2 class="post-title"><a href="blog-single.html">Group Session Moments</a></h2>
                                <ul class="post-meta h5">
                                    <li>August 18, 2016</li>
                                </ul>
                            </div>
                            <div class="post-content">
                                <p>Marianne or husbands if at stronger ye. Considered is as middletons uncommonly. Promotion perfectly ye consisted so. His chatty dining for effect ladies active.</p>
                            </div>
                            <div class="post-more"><a href="#">Read More →</a></div>
                        </div>
                    </article>
                    <!-- Post end-->
                </div>
                <div class="col-md-4 post-item">

                    <!-- Post-->
                    <article class="post">
                        <div class="post-preview">
                            <a href="#">
                                <img src="assets/images/blog/2.jpg" alt=""></a>
                        </div>
                        <div class="post-wrapper">
                            <div class="post-header">
                                <h2 class="post-title"><a href="blog-single.html">Minimalist Chandelier</a></h2>
                                <ul class="post-meta h5">
                                    <li>August 18, 2016</li>
                                </ul>
                            </div>
                            <div class="post-content">
                                <p>Depending listening delivered off new she procuring satisfied sex existence. Person plenty answer to exeter it if. Law use assistance especially resolution.</p>
                            </div>
                            <div class="post-more"><a href="#">Read More →</a></div>
                        </div>
                    </article>
                    <!-- Post end-->
                </div>
                <div class="col-md-4 post-item">

                    <!-- Post-->
                    <article class="post">
                        <div class="post-preview">
                            <a href="#">
                                <img src="assets/images/blog/3.jpg" alt=""></a>
                        </div>
                        <div class="post-wrapper">
                            <div class="post-header">
                                <h2 class="post-title"><a href="blog-single.html">Green Land Sport Season</a></h2>
                                <ul class="post-meta h5">
                                    <li>August 18, 2016</li>
                                </ul>
                            </div>
                            <div class="post-content">
                                <p>Marianne or husbands if at stronger ye. Considered is as middletons uncommonly. Promotion perfectly ye consisted so. His chatty dining for effect ladies active.</p>
                            </div>
                            <div class="post-more"><a href="#">Read More →</a></div>
                        </div>
                    </article>
                    <!-- Post end-->
                </div>
            </div>
            <div class="row m-t-50">
               <%-- <div class="col-md-12">
                    <div class="text-center"><a class="btn btn-lg btn-circle btn-brand" href="#">Visit blog</a></div>
                </div>
            </div>
        </div>
    </section>--%>
    <!-- News end-->

    <!-- Contact-->
    <section class="module" style="background-image: url(img/newsletter.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-8 m-auto">
                    <div class="module-title text-center" style="margin:0px 0px 30px 0px">
                        <h2 style="color:#ffffff">Newsletter</h2>
                        <p class="font-serif" style="color:#ffffff">Bądź na bieżąco z najlepiej dopasowanymi do Twoich potrzeb ofertami</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 m-auto">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" name="name" placeholder="Imię" required="">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="text" name="surname" placeholder="Nazwisko" required="">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input class="form-control" type="email" name="email" placeholder="E-mail" required="">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <%--<div class="col-md-12">
											<div class="form-group">
												<input class="form-control" type="text" name="subject" placeholder="Subject" required="">
												<p class="help-block text-danger"></p>
											</div>
										</div>
										<div class="col-md-12">
											<div class="form-group">
												<textarea class="form-control" name="message" placeholder="Message" rows="12" required=""></textarea>
											</div>
										</div>--%>
                    </div>
                   
                </div> 
            </div><div class="row text-center">
                        <div class="col-md-12 text-center" style="margin-top: 20px;"> 
                            <div class="text-center">
                                <input style="background-color:#094d9c; border-color:#094d9c" 
                                    class="btn btn-round btn-brand" type="submit" value="Zapisz się">
                            </div>
                        </div>
                    </div>
        </div>
    </section>
    <!-- Contact end-->

    <%--<!-- Layout-->
    <div class="layout">
        <!-- Wrapper-->
        <div class="wrapper">


            <!-- Page Header end-->





        </div>
        <!-- Wrapper end-->

    </div>
    <!-- Layout end-->--%>
</asp:Content>
