﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UniSheet.aspx.cs" Inherits="MultiLoans.UniSheet" %>


<asp:Content ID="head" runat="server" ContentPlaceHolderID="Head">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</asp:Content>
<asp:Content ID="bodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .ui-slider .ui-slider-handle {
            width:2em;
            left:-.6em;
            text-decoration:none;
            text-align:center;
        }
        /*.ui-state-default, .ui-widget-content .ui-state-default
        {
            background:blue !important;
        }
       .ui-state-default, .ui-widget-content .ui-state-hover
        {
            background:red !important;
        }*/
    </style>
    <script>
        //$(function () {
        //    $("#slider").slider({
        //        range: "max",
        //        min: 0, // min value
        //        max: 200, // max value
        //        step: 0.1,
        //        value: 100,
        //        change: $(function (event, ui) {
        //            alert(slider.value);
        //        })
        //    });
        //});
        $(function () {
            $("#slider").slider(
           {
               value: 100,
               min: 0,
               max: 500,
               step: 50,
               //slide: function (event, ui) {
               //    var val = $('#slider').slider("option", "value");
               //    console.log('slide');
               //    console.log(val)
               //},
               change: function (event, ui) {
                    var val = $('#slider').slider("option", "value");
                    console.log('change');
                    console.log(val);
                    $('#txbSliderValue').val(val)
               }
            }
           );
        });        
  </script>
    <div class="row">
        <div class="col-md-6">
            <h2>Wybierz kwotę</h2>
            <div style="margin-top:30px" class="ui-slider ui-slider-handle" id="slider"></div>
    <asp:TextBox ID="txbSliderValue" runat="server" ClientIDMode="Static" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-6">
            <h2>Get more libraries</h2>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
            </p>
        </div>
    </div>
    
</asp:Content>
